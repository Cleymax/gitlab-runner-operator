import os


def resove_file_path(file):
    dirname = os.path.dirname(__file__)
    return os.path.join(dirname, file)
