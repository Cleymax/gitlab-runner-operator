import os as _os
import sys
from pathlib import Path
import fileutil

ARCH_AMD64 = "amd64"
ARCH_PPC64LE = "ppc64le"


def certified():
    return _os.getenv("CERTIFIED", "false") in ["true", "True", "1"]


def operator_version(clean=False):
    if len(sys.argv) < 2:
        return "v0.0.1"

    clean_version = sys.argv[1].replace('v', '')
    if clean:
        return clean_version

    return 'v' + clean_version


def previous_version(clean=False):
    prev = _os.getenv("PREV_VERSION")

    if prev:
        if clean:
            return prev.replace('v', '')
        else:
            return prev
    else:
        return None


def platform():
    return _os.getenv("PLATFORM", "linux/amd64")


def platforms():
    return _os.getenv("PLATFORMS", platform()).split(",")


def arch():
    return _os.getenv("ARCH", platform().split("/")[1])


def os():
    return _os.getenv("OS", platform().split("/")[0])


def helper_arch():
    helper_arch = arch()
    if helper_arch == ARCH_AMD64:
        return "x86_64"

    return helper_arch


def runner_revision():
    if len(sys.argv) < 3:
        return "v" + Path(fileutil.resove_file_path("../../APP_VERSION")).read_text()

    return sys.argv[2]


def upstream_ubi_images_repository():
    return _os.getenv("GITLAB_RUNNER_UBI_IMAGES_REGISTRY", "registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images")


def upstream_operator_images_repository():
    return _os.getenv("GITLAB_RUNNER_OPERATOR_REGISTRY", "registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator")


def kube_rbac_proxy_image():
    if not certified():
        return _os.getenv("KUBE_RBAC_PROXY_IMAGE", "registry.redhat.io/openshift4/ose-kube-rbac-proxy:v4.8.0")

    return _os.getenv("KUBE_RBAC_PROXY_IMAGE", "registry.redhat.io/openshift4/ose-kube-rbac-proxy@sha256:71527b0033776ac8a68e9c52937cbdac5909a56058105bcb6489c4a0cb9e4f4d")


def opm_binary_image(arch):
    base_image = "registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/openshift4/ose-operator-registry:"
    if arch == ARCH_AMD64:
        return _os.getenv("OPM_BINARY_IMAGE_AMD64", base_image + ARCH_AMD64)
    elif arch == ARCH_PPC64LE:
        return _os.getenv("OPM_BINARY_IMAGE_PPC64LE", base_image + ARCH_PPC64LE)
    else:
        raise Exception("Unknown arch: {}".format(arch))
